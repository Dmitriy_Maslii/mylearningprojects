﻿using System;
using System.Collections.Generic;
using System.Linq;
using SqlOleDb;
using DomainEntities;
using System.Configuration;

namespace ProductManager
{
    class Program    
    {
        private static string _connectionString;

        private static readonly Dictionary<string, Action<string, int>> _commands = new Dictionary<string, Action<string, int>>()
        {
            {"add", AddProduct},
            {"del", RemoveProduct},
            {"change", ChangeQuantity}
            
        };

        static void Main(string[] args)
        {
            _connectionString = ConfigurationManager.ConnectionStrings["DefaultDbConnection"].ConnectionString;

            PrintList();

            var availableCommands = $"\nAvailable commands: {string.Join(", ", _commands.Keys)}\n";
            Console.WriteLine(availableCommands);

            var userInput = "";
            
            while (userInput.ToLower() != "exit")
            {
                userInput = Console.ReadLine();

                if(!userInput.Trim().Contains(" "))
                {
                    Console.WriteLine(availableCommands);
                    continue;
                }

                var command = userInput.Split()[0].ToLower();
                var name = userInput.Split()[1];
                int quantity = 0;

                if(userInput.Split().Length == 3)
                {
                    if (!int.TryParse(userInput.Split()[2], out quantity)){
                        Console.WriteLine(availableCommands);
                    }
                }
                
                if (_commands.ContainsKey(command))
                {
                    _commands[command](name, quantity);
                    PrintList();
                }
            }
        }

        private static void ChangeQuantity(string name, int quantity)
        {
            var repository = new ProductRepository(_connectionString);

            repository.changeQuantity(name, quantity);
        }

        private static void PrintList()
        {
            var repository = new ProductRepository(_connectionString);

            var products = repository.GetAll().ToList();

            Console.WriteLine($"\nProduct count: {products.Count}\n");

            products.ForEach((product) => Console.WriteLine(product));

            Console.WriteLine();
        }

        private static void AddProduct(string name, int quantity)
        {
            var repository = new ProductRepository(_connectionString); 

            var entities = repository.GetAll().ToList();

            foreach(var entity in entities)
            {
                if(entity.Name == name)
                {
                    Console.WriteLine($"{name} already in the database.");
                    return;
                }
            }

            var newProduct = new Product()
            {
                Name = name,
            };

            if(quantity != -1)                       //TODO: Check for -1 is useless 
            {
                newProduct.Quantity = quantity;
            }

            repository.Add(newProduct);
        }

        private static void RemoveProduct(string name, int quantity)
        {
            var repository = new ProductRepository(_connectionString);

            repository.RemoveByName(name);
        }
    }
}
