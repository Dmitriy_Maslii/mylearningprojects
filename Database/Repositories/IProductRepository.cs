﻿using System;
using DomainEntities;

namespace Repositories
{
    public interface IProductRepository : IRepository<Product, Guid>
    {
    }
}
