﻿using System.Linq;
using DomainEntities;

namespace Repositories
{
    public interface IRepository<T, Tid> where T : IEntityBase<Tid> 
    {
        T Get(object id);

        IQueryable<T> GetAll();

        void Remove(object id);

        T Add(T entity);

        T Update(T entity);
    }
}
