using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Telegram.Bot;
using Telegram.Bot.Types;
using SqlEntityFramework;
using DomainEntities;

namespace TelegramBotProductManagerEntityFramework
{
    public class Program
    {
        private static TelegramBotClient _telegramBot;
        private static string _connectionString;

        private static readonly Dictionary<string, Action<string, int>> _commands = new Dictionary<string, Action<string, int>>
        {                                                                         
            { "/add", AddProduct },                                               
            { "/delete", RemoveProduct },
            { "/change", ChangeQuantity },
        };
        public static IHostBuilder CreateHostBuilder(string[] args) => Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();
            });

        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            InitConfig();

            _telegramBot.OnMessage += Client_OnMessage;
            _telegramBot.StartReceiving();

            host.Run();
        }

        private static void InitConfig()
        {
            _telegramBot = new TelegramBotClient(Startup.Configuration.GetValue<string>("TelegramBotId")); //TODO: Fix TelegramBotId
            _connectionString = Startup.Configuration.GetConnectionString("DefaultDbConnection");
        }

        private static void Client_OnMessage(object sender, Telegram.Bot.Args.MessageEventArgs e)
        {
            var text = e.Message.Text;
            text = text.Trim();

            if (string.IsNullOrEmpty(text))
            {
                return;
            }

            if (!text.Contains(" "))
            {
                _telegramBot.SendTextMessageAsync(e.Message.Chat, "Invalid command.");
                PrintList(e.Message.Chat);
                return;
            }

            var command = text.Split()[0].ToLower();
            var nameProduct = text.Split()[1];
            var quantity = 0;

            if (text.Split().Length == 3)
            {
                if (!int.TryParse(text.Split()[2], out quantity))
                {
                    _telegramBot.SendTextMessageAsync(e.Message.Chat, "Invalid command. Example: [command] [product name] [quantity]");
                    return;
                }
            }

            if (_commands.ContainsKey(command))
            {
                try
                {
                    _commands[command](nameProduct, quantity);
                }
                catch (Exception ex)
                {
                    _telegramBot.SendTextMessageAsync(e.Message.Chat, $"Something wrong. {ex.Message}");
                }
                PrintList(e.Message.Chat);
            }
            else
            {
                _telegramBot.SendTextMessageAsync(e.Message.Chat, $"Invalid command. Available commands: {string.Join(", ", _commands.Keys)}");
                return;
            }
        }

        private static void PrintList(Chat chat)
        {
            var repository = new ProductRepository(_connectionString);

            var products = repository.GetAll().ToList();

            var message = $"{Environment.NewLine}Product count: {products.Count}{Environment.NewLine}";

            int index = 0;
            products.ForEach(product => message += $"{Environment.NewLine}{++index}) {product.ToString()}");

            message += Environment.NewLine;

            _telegramBot.SendTextMessageAsync(chat, message);
        }




        private static void AddProduct(string name, int quantity)
        {
            var repository = new ProductRepository(_connectionString);

            var entities = repository.GetAll().ToList();

            foreach (var entity in entities)
            {
                if (entity.Name.ToLower() == name.ToLower())
                {
                    throw new Exception($"{name} already in the database.");  // TODO: ��� ���������� ���� �� ������ ������ ���������� ����������
                }
            }

            var newProduct = new Product()
            {
                Name = name,
                Quantity = quantity,
            };

            repository.Add(newProduct);
        }
        private static void RemoveProduct(string name, int quantity)
        {
            var repository = new ProductRepository(_connectionString);

            repository.RemoveByName(name);
        }
        private static void ChangeQuantity(string name, int quantity)
        {
            var repository = new ProductRepository(_connectionString);

            repository.ChangeQuantity(name, quantity);
        }
    }
}
