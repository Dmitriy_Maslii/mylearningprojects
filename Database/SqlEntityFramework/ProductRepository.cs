﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainEntities;

namespace SqlEntityFramework
{
    public class ProductRepository
    {
        private readonly string _connectionString;

        public ProductRepository(string connectionString)
        {
            _connectionString = connectionString;
        }
        public Product Add(Product entity)
        {
            using (var context = new ProductContext(_connectionString))
            {
                SetCreatedOn(entity);
                context.Products.Add(entity);
                context.SaveChanges();

                return entity;                                        //not necessary
            }
        }

        public void RemoveByName(string name)
        {
            using (var context = new ProductContext(_connectionString))
            {
                var entityToRemove = context.Products.Where(p => p.Name == name).FirstOrDefault();              //what mean default value?

                if(entityToRemove == null)
                {
                    throw new Exception("Product with this name not found.");
                }

                context.Products.Remove(entityToRemove);
                context.SaveChanges();
            }            
        }

        private static void SetCreatedOn(Product entity)
        {
            if (entity != null)
            {
                entity.CreatedOn = DateTime.Now;
            }
        }
        private static void SetUpdatedOn(Product entity)
        {
            if (entity != null)
            {
                entity.UpdatedOn = DateTime.Now;
            }
        }
        //public Product Get(object id)
        //{
        //    using (OleDbConnection connection = new OleDbConnection(ConnectionString))
        //    {
        //        var cmd = connection.CreateCommand();

        //        cmd.CommandText = $"select top 1 * from {TableName} where id = '{id}'";

        //        connection.Open();

        //        var reader = cmd.ExecuteReader();

        //        if (!reader.Read())
        //        {
        //            throw new Exception("can't read");
        //        }

        //        var result = new Product(reader.GetGuid(0));

        //        result.Name = reader.GetString(1);
        //        result.Quantity = reader.GetInt32(2);
        //        result.CreatedOn = reader.GetDateTime(3);
        //        result.UpdatedOn = reader.GetDateTime(4);

        //        connection.Close();

        //        return result;
        //    }
        //}

        public Product Get(string name)
        {
            using (var context = new ProductContext(_connectionString))
            {
                var entity = context.Products.Where(p => p.Name == name).FirstOrDefault();

                if (entity == null)
                {
                    throw new Exception("Product with this name not found.");
                }

                return entity;
            }
        }

        public IQueryable<Product> GetAll()
        {
            using (var context = new ProductContext(_connectionString))
            {
                var products = new List<Product>();                 //
                products = context.Products.ToList();               //ВОЗМОЖНО ЭТО КОСТЫЛИ
                return products.AsQueryable();                      //
            }
        }

        //public void Remove(object id)
        //{
        //    using (OleDbConnection connection = new OleDbConnection(ConnectionString))
        //    {
        //        var cmd = connection.CreateCommand();

        //        cmd.CommandText = $"delete from {TableName} where id='{id}'"; // SQL не регистро зависимый, а имена колонок тоже??

        //        connection.Open();

        //        var result = cmd.ExecuteNonQuery();

        //        connection.Close();

        //        if (result == 0)
        //        {
        //            throw new Exception("0 rows affected");
        //        }
        //    }
        //}

        public DateTime? ChangeQuantity(String name, int quantity)
        {
            using (var context = new ProductContext(_connectionString))
            {
                var entityToChange = context.Products.Where(p => p.Name == name).FirstOrDefault();

                if(entityToChange == null)
                {
                    throw new Exception("Product with this name not found.");
                }

                SetUpdatedOn(entityToChange);
                entityToChange.Quantity = quantity;
                context.SaveChanges();

                return entityToChange.UpdatedOn;
            }
        }
    }
}
