﻿using System.Data.Entity;
using DomainEntities;

namespace SqlEntityFramework
{
    public class ProductContext : DbContext
    {
        public ProductContext(string connectionString)
            : base(connectionString)
        { 
        }

        public DbSet<Product> Products { get; set; }
        

    }
}
