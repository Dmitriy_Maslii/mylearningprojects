﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using DomainEntities;
using Repositories;

namespace SqlOleDb
{
    public class ProductRepository : IProductRepository                 
    {
        private const string TableName = "Products";

        private readonly string ConnectionString;

        public ProductRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentException(nameof(connectionString));
            }

            ConnectionString = connectionString;
        }


        public Product Add(Product entity)
        {
            using(OleDbConnection connection = new OleDbConnection(ConnectionString))
            {
                var cmd = connection.CreateCommand();

                SetCreatedOn(entity); 

                cmd.CommandText = $"insert into {TableName} (Id, Name, Quantity, CreatedOn, UpdatedOn)" +
                    $" values('{entity.Id}', '{entity.Name}','{entity.Quantity}','{entity.CreatedOn}','{entity.UpdatedOn}')";

                connection.Open();

                var result = cmd.ExecuteNonQuery();

                connection.Close();

                if(result == 0)
                {
                    throw new Exception("0 rows affected");
                }

                return Get(entity.Id); // Зачем создавать такой же обьект, а не вернуть тот что пришел? и вообще зачем его возвращать?
            }
        }

        public void RemoveByName(string name)
        {
            using(OleDbConnection connection = new OleDbConnection(ConnectionString))
            {
                var cmd = connection.CreateCommand();

                cmd.CommandText = $"delete from {TableName} where Name='{name}'"; // SQL не регистро зависимый, а имена колонок тоже??

                connection.Open();

                var result = cmd.ExecuteNonQuery();

                connection.Close();

                if(result == 0)
                {
                    throw new Exception("0 rows affected");
                }
            }
        }

        private static void SetCreatedOn(Product entity)
        {
            if (entity != null)
            {
                entity.CreatedOn = DateTime.Now;
            }
        }
        private static void SetUpdatedOn(Product entity)
        {
            if (entity != null)
            {
                entity.UpdatedOn = DateTime.Now;
            }
        }
        public Product Get(object id)
        {
            using(OleDbConnection connection = new OleDbConnection(ConnectionString))
            {
                var cmd = connection.CreateCommand();

                cmd.CommandText = $"select top 1 * from {TableName} where id = '{id}'";

                connection.Open();

                var reader = cmd.ExecuteReader();

                if(!reader.Read())
                {
                    throw new Exception("can't read");
                }

                var result = new Product(reader.GetGuid(0));

                result.Name = reader.GetString(1);
                result.Quantity = reader.GetInt32(2);
                result.CreatedOn = reader.GetDateTime(3);
                result.UpdatedOn = reader.GetDateTime(4);

                connection.Close();

                return result;
            }
        }

        public Product Get(string name)
        {
            using (OleDbConnection connection = new OleDbConnection(ConnectionString))
            {
                var cmd = connection.CreateCommand();

                cmd.CommandText = $"select top 1 * from {TableName} where name = '{name}'";

                connection.Open();

                var reader = cmd.ExecuteReader();

                if (!reader.Read())
                {
                    throw new Exception("can't read");
                }

                var result = new Product(reader.GetGuid(0));

                result.Name = reader.GetString(1);
                result.Quantity = reader.GetInt32(2);
                result.CreatedOn = reader.GetDateTime(3);
                result.UpdatedOn = reader.GetDateTime(4);

                connection.Close();

                return result;
            }
        }

        public IQueryable<Product> GetAll()
        {
            var result = new List<Product>();

            using(OleDbConnection connection = new OleDbConnection(ConnectionString))
            {
                var cmd = connection.CreateCommand();

                cmd.CommandText = $"select * from {TableName}";

                connection.Open();

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var current = new Product(reader.GetGuid(0));

                    current.Name = reader.GetString(1);
                    current.Quantity = reader.GetInt32(2);
                    current.CreatedOn = reader.GetDateTime(3);
                    current.UpdatedOn = reader.GetDateTime(4);

                    result.Add(current);
                }

                connection.Close();

                return result.AsQueryable();
            }
        }

        public void Remove(object id)
        {
            using(OleDbConnection connection = new OleDbConnection(ConnectionString))
            {
                var cmd = connection.CreateCommand();

                cmd.CommandText = $"delete from {TableName} where id='{id}'"; // SQL не регистро зависимый, а имена колонок тоже??

                connection.Open();

                var result = cmd.ExecuteNonQuery();

                connection.Close();

                if (result == 0)
                {
                    throw new Exception("0 rows affected");
                }
            }
        }

        public void changeQuantity(String name, int quantity)
        {
            var entityToUpdate = Get(name);

            entityToUpdate.Quantity = quantity;

            SetUpdatedOn(entityToUpdate);

            Update(entityToUpdate);            
        }

        public Product Update(Product entity)
        {
            using (OleDbConnection connection = new OleDbConnection(ConnectionString))
            {
                var cmd = connection.CreateCommand();

                cmd.CommandText = $"update {TableName} set quantity='{entity.Quantity}', updatedon='{entity.UpdatedOn}' where id='{entity.Id}'";

                connection.Open();

                var result = cmd.ExecuteNonQuery();

                connection.Close();

                if (result == 0)
                {
                    throw new Exception("0 rows affected");
                }
            }
            return entity;
        }
    }
}
