﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainEntities
{
    public interface ICreatable
    {
        DateTime? CreatedOn { get; set; }

        
    }
}
