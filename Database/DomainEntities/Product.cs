﻿using System;

namespace DomainEntities
{
    public class Product : IEntityBase<Guid>
    {
        public Product()
        {
            Id = Guid.NewGuid();
        }

        public Product(Guid id)
        {
            Id = id;
        }

        public string Name { get; set; }

        public int Quantity { get; set; } = 1;
        public Guid Id { get; private set; }
        public DateTime? UpdatedOn { get; set; } = null; 
        public DateTime? CreatedOn { get; set; }
        public override string ToString() 
        {
            string curentUpdatedOn;

            if(UpdatedOn == null || UpdatedOn.Value == DateTime.MinValue)     
            {
                curentUpdatedOn = "-";
            }
            else
            {
                curentUpdatedOn = $"{UpdatedOn:yyyy-MM-dd hh:mm}";
            }

            return $" Name: {Name}; Quantity: {Quantity}; Created On: {CreatedOn:yyyy-MM-dd hh:mm}; Updated On: {curentUpdatedOn};";
        }
    }
}
