﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SqlEntityFramework;
using DomainEntities;

namespace ProductManagerWinForms
{
    public partial class DeleteForm : Form
    {
        private readonly List<DataGridViewRow> _dataGridViewRows;
        private readonly ProductRepository _repository;
        public DeleteForm(ProductRepository repository, List<DataGridViewRow> dataGridViewRows)
        {
            _repository = repository;
            _dataGridViewRows = dataGridViewRows;
           
            InitializeComponent();

            LabelDelete.Text = $"Вы действительно хотите удалить\n {dataGridViewRows.Count} продукт(-а,-ов)";
        }

        private void buttonOkDelete_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < _dataGridViewRows.Count; i++)
            {
                _repository.RemoveByName(_dataGridViewRows[i].Cells["NameOfProduct"].Value.ToString());
            }

            this.Close();
        }

        private void CancelDelete_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
