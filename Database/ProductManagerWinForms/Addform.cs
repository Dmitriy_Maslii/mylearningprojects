﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SqlEntityFramework;
using DomainEntities;

namespace ProductManagerWinForms
{
    public partial class AddForm : Form
    {
        private ProductRepository _repository;
        
        public AddForm(ProductRepository repository)
        {
            _repository = repository;

            InitializeComponent();

        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            int quantityProducts;

            if (!string.IsNullOrEmpty(textBoxQuantity.Text) && !string.IsNullOrEmpty(textBoxName.Text)
                && int.TryParse(textBoxQuantity.Text, out quantityProducts))
            {
                var newProduct = new Product()
                {
                    Name = this.textBoxName.Text,
                    Quantity = quantityProducts
                };

                _repository.Add(newProduct);



                this.Close();
            }
        }

        private void textBoxQuantity_TextChanged(object sender, EventArgs e)
        {
            int num;
            var result = int.TryParse(textBoxQuantity.Text, out num);
            if (!result)
            {
                errorProviderQuantiry.SetError(textBoxQuantity, "Must contain only numbers");
            }
            else
            {
                errorProviderQuantiry.Clear();
            }
        }
    }
}
