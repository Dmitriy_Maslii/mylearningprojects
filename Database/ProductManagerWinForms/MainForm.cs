﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using SqlEntityFramework;
using DomainEntities;

namespace ProductManagerWinForms
{
    public partial class MainForm : Form
    {
        private ProductRepository _repository;
        private string _connectionString = "Data Source=DMITRIY-LAPTOP\\SQLEXPRESS;Initial Catalog = products_EF; Integrated Security = True";
                                                       //Why do not work ConfigurationManager?


        public MainForm()
        {
            _repository = new ProductRepository(_connectionString);
            
            InitializeComponent();
            RefreshDataGridView();
        }

        public void RefreshDataGridView(List<Product> products = null)
        {
            if (products == null)
            {
                dataGridView.DataSource = _repository.GetAll().ToList();
            }
            else
            {
                dataGridView.DataSource = products;
            }
            dataGridView.Columns["Id"].Visible = false;
        }

        private void dataGridView_CellParsing(object sender, DataGridViewCellParsingEventArgs e)
        {
            int newQuantity = int.Parse(e.Value.ToString());

            var nameOfChangingProduct = dataGridView.CurrentRow.Cells["NameOfProduct"].Value.ToString();

            if (newQuantity != _repository.Get(nameOfChangingProduct).Quantity)
            {
                dataGridView.CurrentRow.Cells["UpdatedOn"].Value =
                    _repository.ChangeQuantity(nameOfChangingProduct, newQuantity);
            }            
        }

        private void dataGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            int num;
            var result = int.TryParse(e.FormattedValue.ToString(), out num);
            if (!result)
            {
                dataGridView.CancelEdit();
            }
        }

        private void buttonAddProduct_Click(object sender, EventArgs e)
        {
            var addForm = new AddForm(_repository);
            var result = addForm.ShowDialog(this);

            RefreshDataGridView(); //TODO: refresh only in case data really changed, not when just closed "AddProduct"
        }

        private void buttonDeleteProduct_Click(object sender, EventArgs e)
        {
            //var selectedRows = dataGridView.SelectedRows;
            
            //for(int i = 0; i < selectedRows.Count; i++)
            //{
            //    _repository.RemoveByName(selectedRows[i].Cells["NameOfProduct"].Value.ToString());
            //}



            var selectedCells = dataGridView.SelectedCells;
            var owningRowsSelectedCells = new List<DataGridViewRow>();

            for (int i = 0; i < selectedCells.Count; i++)
            {
                var owningRow = selectedCells[i].OwningRow;

                if (!owningRowsSelectedCells.Contains(owningRow))
                {
                    owningRowsSelectedCells.Add(owningRow);
                }                
            }

            var deleteForme = new DeleteForm(_repository, owningRowsSelectedCells);
            var result = deleteForme.ShowDialog(this);



            RefreshDataGridView(); //TODO: refresh only in case data really changed
        }

        private void textBoxFilter(object sender, EventArgs e)
        {
            var productList = _repository.GetAll().ToList();
            
            var filtered = productList.Where(x => x.Name.ToUpper().Contains(textBoxSearch.Text.ToUpper()))
                .ToList();
            RefreshDataGridView(filtered);
        }
    }
}
