﻿namespace ProductManagerWinForms
{
    partial class DeleteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonOkDelete = new System.Windows.Forms.Button();
            this.CancelDelete = new System.Windows.Forms.Button();
            this.LabelDelete = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonOkDelete
            // 
            this.buttonOkDelete.Location = new System.Drawing.Point(12, 111);
            this.buttonOkDelete.Name = "buttonOkDelete";
            this.buttonOkDelete.Size = new System.Drawing.Size(130, 30);
            this.buttonOkDelete.TabIndex = 0;
            this.buttonOkDelete.Text = "OK";
            this.buttonOkDelete.UseVisualStyleBackColor = true;
            this.buttonOkDelete.Click += new System.EventHandler(this.buttonOkDelete_Click);
            // 
            // CancelDelete
            // 
            this.CancelDelete.Location = new System.Drawing.Point(200, 111);
            this.CancelDelete.Name = "CancelDelete";
            this.CancelDelete.Size = new System.Drawing.Size(130, 30);
            this.CancelDelete.TabIndex = 1;
            this.CancelDelete.Text = "Отмена";
            this.CancelDelete.UseVisualStyleBackColor = true;
            this.CancelDelete.Click += new System.EventHandler(this.CancelDelete_Click);
            // 
            // LabelDelete
            // 
            this.LabelDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelDelete.Location = new System.Drawing.Point(12, 24);
            this.LabelDelete.Name = "LabelDelete";
            this.LabelDelete.Size = new System.Drawing.Size(318, 64);
            this.LabelDelete.TabIndex = 2;
            this.LabelDelete.Text = "label1";
            this.LabelDelete.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DeleteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(342, 153);
            this.Controls.Add(this.LabelDelete);
            this.Controls.Add(this.CancelDelete);
            this.Controls.Add(this.buttonOkDelete);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "DeleteForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Удалить";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonOkDelete;
        private System.Windows.Forms.Button CancelDelete;
        private System.Windows.Forms.Label LabelDelete;
    }
}