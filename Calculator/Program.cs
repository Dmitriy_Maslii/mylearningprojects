﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            String result;

            result = Calculator.Calculate("2 ^ 3 * 5,6 ^ 5");
            Debug.Assert(result == "44058,542079999985");

            result = Calculator.Calculate("2, 5 ^ 5 / 2");
            Debug.Assert(result == "48,828125");

            result = Calculator.Calculate("1 - 5 /4 * 1,8");
            Debug.Assert(result == "-1,25");

            result = Calculator.Calculate("1 - 5 *4 / 1,8");
            Debug.Assert(result == "-10,11111111111111");

            result = Calculator.Calculate("1 - 5 /4 * -1,8");
            Debug.Assert(result == "3,25");

            result = Calculator.Calculate("-1 * -3");
            Debug.Assert(result == "3");

            result = Calculator.Calculate("1 - 5 / 4 * 1,8 + (2 + 4)");
            Debug.Assert(result == "4,75");

            result = Calculator.Calculate("(1 + 2) * ((2 * 5 - 2) + (8 / 4 * 3) / 2 + 5) - 5 * (-1)");
            Debug.Assert(result == "53");

            result = Calculator.Calculate("-3 - 1");
            Debug.Assert(result == "-4");

            result = Calculator.Calculate("6 - 3 * -1");
            Debug.Assert(result == "9");

            result = Calculator.Calculate("-5-(3-5)");
            Debug.Assert(result == "-3");

            result = Calculator.Calculate("(-1 - 5)");
            Debug.Assert(result == "-6");

            result = Calculator.Calculate("1 - 5 / 4 * 1,8 + 2");
            Debug.Assert(result == "0,75");

            result = Calculator.Calculate("-1 - 5 / 4 * 1,8 + 2");
            Debug.Assert(result == "-1,25");

            result = Calculator.Calculate("-1 - 5 / 4 * 1,8 + 2^3");
            Debug.Assert(result == "4,75");

            result = Calculator.Calculate("2^3^2");
            Debug.Assert(result == "64");

            result = Calculator.Calculate("(1 / (10 + 5) - 4) * (7 / (4 - 2))");
            Debug.Assert(result == "-13,766666666666666");

            result = Calculator.Calculate("1 - 5 / 4 * 1,8 + (2 + 4^3)");
            Debug.Assert(result == "64,75");

            while (true)
            {
                try
                {
                    var inputString = Console.ReadLine();
                    result = Calculator.Calculate(inputString);
                    Console.WriteLine(inputString + " = " + result);
                }
                catch (ArgumentException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                catch(DivideByZeroException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                catch
                {
                    Console.WriteLine("Other exception.");
                }
            }
        }
    }
}
