﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;

namespace FootballGame
{
    public static class GameManager
    {
        public static List<Player> homeTeamPlayers = new List<Player>()
        {
            new Player("Messi", Role.Forward, 90, 27, 89),
            new Player("Luis Suarez", Role.Forward, 90, 52, 80),
            new Player("Dembele", Role.Forward, 79, 36, 93),
            new Player("Rakitic", Role.Midfielder, 85, 73, 63),
            new Player("Busquets", Role.Midfielder, 62, 85, 43),
            new Player("Coutinho", Role.Midfielder, 81, 45, 81),
            new Player("Sergi Roberto", Role.Defender, 63, 80, 78),
            new Player("Pique", Role.Defender, 61, 87, 55),
            new Player("Umtiti", Role.Defender, 64, 88, 72),
            new Player("Jordi Alba", Role.Defender, 69, 79, 93),
            new Player("Ter Stegen", Role.Goalkeeper, 100, 90, 0), //исправить костыль (навык удара для вратарей) 
        };
        
        public static List<Player> guestTeamPlayers = new List<Player>() 
        {
            new Player("Gotze", Role.Forward, 74, 60, 70),
            new Player("Hazard", Role.Midfielder, 82, 35, 91),
            new Player("Reus", Role.Midfielder, 88, 46, 86),
            new Player("Sancho", Role.Midfielder, 74, 37, 91),
            new Player("Brandt", Role.Midfielder, 77, 30, 81),
            new Player("Witsel", Role.Midfielder, 74, 81, 71), 
            new Player("Schulz", Role.Defender, 52, 73, 88),
            new Player("Hummels", Role.Defender, 58, 90, 65),
            new Player("Akanji", Role.Defender, 42, 78, 80),
            new Player("Piszczek", Role.Defender, 69, 82, 81),
            new Player("Burki", Role.Goalkeeper, 100, 87, 0), //исправить костыль (навык удара для вратарей) 
        };
        
        public static Team homeTeam = new Team("Barca", homeTeamPlayers);
        public static Team guestTeam = new Team("Borussia dortmund", guestTeamPlayers);

        static Random random = new Random();
        public static Player playerWithBall = null;

        public static int ScoreHomeTeam = 0;
        public static int ScoreGuestTeam = 0;

        public static bool BreakFlag = true;

        public static DateTime TimeStartGame;
        public static DateTime TimeStartSecondHalf;

        private static Timer timerFirsHalf = null;
        private static Timer timerSecondHalf = null;

        public static void GameProcess() 
        {
            while (true)
            {
                 ActionSelection();
                
                if (!BreakFlag)
                {
                    MessageWriter.WriteBreak();
                    
                    break;
                }
            }
            
            Thread.Sleep(3000);
            
            MessageWriter.WriteStartSecondHalf();
            GameTimerSecondHalf();
            TimeStartSecondHalf = DateTime.Now;
            PutBallIntoGame(guestTeam);

            while (true)
            {
                ActionSelection();

                if (BreakFlag)
                {
                    MessageWriter.WriteFinisheGame(); 

                    break;
                }
            }
        }
        public static void GameTimerFirstHalf()
        {
            timerFirsHalf = new Timer((o) => BreakFlag = false, null, 100000, -1);
        }                                                        // Нужно обьявлять как статическое поле класса что бы GC не удалил
                                                                 // ссылку на таймер после выполнения метода???
        public static void GameTimerSecondHalf()
        {
            timerSecondHalf = new Timer((o) => BreakFlag = true, null, 100000, -1);
        }
        
        public static void StartGame()
        {
            MessageWriter.WriteGreeting();

            homeTeam.playersOfTeam.ForEach(x => x.NotifyPass += InterceptPass);
            guestTeam.playersOfTeam.ForEach(x => x.NotifyPass += InterceptPass);

            homeTeam.playersOfTeam.ForEach(x => x.NotifyDribble += Tackle);
            guestTeam.playersOfTeam.ForEach(x => x.NotifyDribble += Tackle);

            homeTeam.playersOfTeam.ForEach(x => x.NotifyShot += GoalkeeperCatchBall);
            guestTeam.playersOfTeam.ForEach(x => x.NotifyShot += GoalkeeperCatchBall);

            GameTimerFirstHalf();

            TimeStartGame = DateTime.Now;

            PutBallIntoGame(homeTeam);

            GameProcess();
        }
        
        public static void PutBallIntoGame(Team team)
        {
            playerWithBall = team.playersOfTeam.Where(x => x.Role == Role.Midfielder).First();            

            MessageWriter.WritePlayerWhoPutsBallIntoGame();

            playerWithBall = playerWithBall.Pass(team.playersOfTeam.Where(x =>
            x != playerWithBall).ElementAt(random.Next(9)));
        }
        
        public static void ActionSelection()
        {
            var resultRandom = ImprovedRandom.GetRandom(ChanceShotOnGoal(playerWithBall.Role), playerWithBall.BallPossession, playerWithBall.HitAccuracy + 50);
            
            if (resultRandom == 1)
            {
                playerWithBall.ShotOnGoal();
                return;
            }

            if (resultRandom == 2)
            {                
                playerWithBall.Dribble();
                return;
            }

            //pass (добавить приоритет паса вперед и по позициям)
            if (resultRandom == 3)
            {
                if (homeTeam.playersOfTeam.Contains(playerWithBall))
                {
                    playerWithBall = playerWithBall.Pass(homeTeam.playersOfTeam.Where(x => x.Name != playerWithBall.Name).ElementAt(random.Next(10)));
                }
                else
                {
                    playerWithBall = playerWithBall.Pass(guestTeam.playersOfTeam.Where(x => x.Name != playerWithBall.Name).ElementAt(random.Next(10)));
                }
            }
        }

        public static int ChanceShotOnGoal(Role role)
        {
            switch (role)
            {
                case Role.Forward:
                    return 90;
                case Role.Midfielder:
                    return 15;
                case Role.Defender:
                    return 8;
                case Role.Goalkeeper:
                    return 0;
                default:
                    return 100;
            }
        }

        //Получить амплуа игроков которые перехватывают передачу
        //todo: усложнить логику (пример: у Midfielder может перехватить мяч любой игрок)
        public static Role GetRoleForInterceptionPass(Role role)
        {
            switch (role)
            {
                case Role.Forward:
                    return Role.Defender;
                case Role.Midfielder:
                    return Role.Midfielder;
                case Role.Defender:
                    return Role.Forward;
                default:
                    return Role.Forward;
            }
        }

        public static Player DesignatePlayerToDefence()
        {
            var defenseTeam = homeTeam.playersOfTeam.Contains(playerWithBall) ? guestTeam : homeTeam;
            var defensePlayers = defenseTeam.playersOfTeam.Where(x => x.Role == GetRoleForInterceptionPass(playerWithBall.Role));
            return defensePlayers.ElementAt(random.Next(defensePlayers.Count() - 1)); // усложнить логику подбора игрока для защиты
        }

        //Ответные действия противника на действия игрока с мячом
        //Куда их вынести отдельно?
        public static void GoalkeeperCatchBall()
        {
            var goalkeeper = homeTeam.playersOfTeam.Contains(playerWithBall) ?
                guestTeam.playersOfTeam.Where(x => x.Role == Role.Goalkeeper).First() :
                homeTeam.playersOfTeam.Where(x => x.Role == Role.Goalkeeper).First();
                       
            if (ImprovedRandom.GetRandom(goalkeeper.TakeTheBall, playerWithBall.HitAccuracy))
            {
                MessageWriter.WriteGoalkeeperCaughtBall(goalkeeper);
                
                playerWithBall = goalkeeper;
            }
            else
            {
                Team goalConcededTeam;

                if (homeTeam.playersOfTeam.Contains(playerWithBall))
                {
                    ScoreHomeTeam++;
                    goalConcededTeam = guestTeam;
                }
                else
                {
                    ScoreGuestTeam++;
                    goalConcededTeam = homeTeam;
                }
                
                MessageWriter.WriteGoal(playerWithBall);

                PutBallIntoGame(goalConcededTeam);
            }
        }
        public static void Tackle()
        {
            var defensePlayer = DesignatePlayerToDefence();

            int chanceTakeTheBall = defensePlayer.TakeTheBall / 5; // чем на большее число делим тем ниже шанс отнять мяч

            if (ImprovedRandom.GetRandom(chanceTakeTheBall, playerWithBall.BallPossession))
            {
                MessageWriter.WriteTookBallAway(defensePlayer);
                playerWithBall = defensePlayer;
            }            
        }
        public static Player InterceptPass()
        {
            var defensePlayer = DesignatePlayerToDefence();

            int chanceInterceptPass = defensePlayer.TakeTheBall / 5; // чем на большее число делим тем ниже шанс перехватить пас

            if (ImprovedRandom.GetRandom(chanceInterceptPass, playerWithBall.BallPossession))
            { 
                MessageWriter.WriteInterceptPass(defensePlayer);
                return defensePlayer;
            }
            else return null;
        }
    }
}
