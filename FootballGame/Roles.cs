﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FootballGame
{
    public enum Role
    {
        Goalkeeper = 1,

        Defender = 2,

        Midfielder = 3,

        Forward = 4,
    }
}
