﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace FootballGame
{
    public delegate Player MethodIfPass();
    public delegate void MethodIfDribble();
    public delegate void MethodIfShot();
    
    public class Player
    {
        //Игрок имеет: имя, амплуа, навыки (точность удара, отбор, владение);
        //Действия игрока: пас, удар по воротам, вести мяч
                
        public event MethodIfPass NotifyPass;
        public event MethodIfDribble NotifyDribble;
        public event MethodIfShot NotifyShot;

        public Player(string name, Role role, int hitAccuracy, int takeTheBall, int ballPosswssion) 
        { 
            Name = name;
            Role = role;
            HitAccuracy = hitAccuracy;
            TakeTheBall = takeTheBall;
            BallPossession = ballPosswssion;
        }
        
        public string Name { get; }
        
        public Role Role { get; }
        
        public int HitAccuracy { get; }
        
        public int TakeTheBall { get; }
        
        public int BallPossession { get; }
        
        public Player Pass(Player player)
        {
            MessageWriter.WritePass(player);            
            return NotifyPass?.Invoke() ?? player;
        }
        public void Dribble()
        {
            MessageWriter.WriteDribble();
            NotifyDribble?.Invoke();            
        }

        public void ShotOnGoal()
        {
            MessageWriter.WriteShotOnGoal();
            NotifyShot?.Invoke();
        }
    }
}
