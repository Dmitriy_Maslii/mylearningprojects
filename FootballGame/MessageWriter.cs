﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace FootballGame
{
    public static class MessageWriter
    {
        private static ArrayList listPlayersScoringGoal = null;
        private static int switchDribble = 0;

        public static void WriteGreeting()
        {
            Console.WriteLine($"Здравствуйте! Приглашаем вас в Барселону на стадион \"Камп Ноу\".\n");
            Thread.Sleep(1500);
            
            Console.WriteLine($"\t\"{GameManager.homeTeam.Name.ToUpper()}\"   VS   \"{GameManager.guestTeam.Name.ToUpper()}\"\n");
            Thread.Sleep(1500);
            
            Console.WriteLine($"\"{GameManager.homeTeam.Name}\" на правах хозяев поля разыграет мяч.\n");
            Thread.Sleep(1500);
        }

        public static void WritePlayerWhoPutsBallIntoGame()
        {
            Console.WriteLine($"Игрок {GameManager.playerWithBall.Name} будет вводить мяч в игру пассом с центра поля.\n");
            Thread.Sleep(1500);
        }

        public static void WriteGoalkeeperCaughtBall(Player goalkeeper)
        {
            Console.WriteLine($"Вратарь {goalkeeper.Name} поймал мяч.");
            Thread.Sleep(2000);

            Console.WriteLine($"{goalkeeper.Name} вводит мяч в игру.");
            Thread.Sleep(2000);
        }

        public static void WriteGoal(Player player)
        {
            int timeGoal = 0;
            if (GameManager.BreakFlag)
            {
               timeGoal = (int)((DateTime.Now - GameManager.TimeStartGame).TotalMilliseconds * 45.0 / 100000.0);
            }
            else   
            if (!GameManager.BreakFlag)
            {
                timeGoal = (int)((DateTime.Now - GameManager.TimeStartSecondHalf).TotalMilliseconds * 45.0 / 100000.0) + 45;
            }
            
            Console.WriteLine("");
            Console.WriteLine("ГООООООООООООООООЛ!!!\n");
            Thread.Sleep(500);

            Console.WriteLine($"На {timeGoal}' минуте {player.Name} забивает гол!\n");
            Thread.Sleep(500);

            Console.WriteLine($"Счет становится {GameManager.ScoreHomeTeam} : {GameManager.ScoreGuestTeam}.\n");
            Thread.Sleep(2500);

            if(listPlayersScoringGoal == null)
            {
                listPlayersScoringGoal = new ArrayList();
                listPlayersScoringGoal.Add($"{timeGoal, 2}' {player.Name, -12} {GameManager.ScoreHomeTeam} : {GameManager.ScoreGuestTeam}");
            }
            else
            {
                listPlayersScoringGoal.Add($"{timeGoal, 2}' {player.Name, -12} {GameManager.ScoreHomeTeam} : {GameManager.ScoreGuestTeam}");
            }
        }

        public static void WriteTookBallAway(Player player)
        {
            Console.WriteLine($"{player.Name} отбирает мяч у {GameManager.playerWithBall.Name}.");
            Thread.Sleep(1000);
        }

        public static void WriteInterceptPass(Player player)
        {
            Console.WriteLine($"Пас перехватывает {player.Name}.");
            Thread.Sleep(1000);
        }

        public static void WritePass(Player playerToWhomPass)
        {
            Console.WriteLine($"{GameManager.playerWithBall.Name} дает пас {playerToWhomPass.Name}.");
            Thread.Sleep(1000);
        }

        public static void WriteDribble()
        {
            switch (switchDribble)
            {
                case 0:
                    Console.WriteLine($"{GameManager.playerWithBall.Name} на скорости ведет мяч вперед.");
                    switchDribble++;
                    break;
                case 1:
                    Console.WriteLine($"{GameManager.playerWithBall.Name} берет игру на себя.");
                    switchDribble++;
                    break;
                case 2:
                    Console.WriteLine($"{GameManager.playerWithBall.Name} пробрасывает мяч себе на ход.");
                    switchDribble++;
                    break;
                default:
                    Console.WriteLine($"{GameManager.playerWithBall.Name} демонстрирует свои навыки дриблинга.");
                    switchDribble = 0;
                    break;
            }
            Thread.Sleep(1000);            
        }

        public static void WriteShotOnGoal()
        {
            Console.WriteLine($"{GameManager.playerWithBall.Name} бьет по воротам.");
            Thread.Sleep(1000);
        }

        public static void WriteBreak()
        {
            Console.WriteLine();
            Console.WriteLine($"Звучит свисток на перерыв.\n"); 
            Thread.Sleep(500);
            Console.WriteLine($"Счет после первого тайма {GameManager.ScoreHomeTeam} : {GameManager.ScoreGuestTeam}\n\n\n");
            Thread.Sleep(500);
        }

        public static void WriteStartSecondHalf()
        {
            Console.WriteLine("Команды уже на поле и готовы к началу второго тайма.\n");
            Thread.Sleep(1000);
            Console.WriteLine($"Напоминаю что счет после первого тайма был {GameManager.ScoreHomeTeam} : {GameManager.ScoreGuestTeam}\n");
            Thread.Sleep(1000);
            Console.WriteLine("Звучит свисток о начале игры.\n");
            Thread.Sleep(500);
        }

        public static void WriteFinisheGame()
        {
            Console.WriteLine();
            Console.WriteLine("Звучит финальный свисток.\n");
            Thread.Sleep(500);
            Console.WriteLine($"Игра окончена.\n");
            Thread.Sleep(500);
            Console.WriteLine($"{GameManager.homeTeam.Name.ToUpper()} {GameManager.ScoreHomeTeam} : {GameManager.ScoreGuestTeam} {GameManager.guestTeam.Name.ToUpper()}\n");

            if(listPlayersScoringGoal != null)
            {
                for(int i = 0; i < listPlayersScoringGoal.Count; i++)
                {
                    Console.WriteLine(listPlayersScoringGoal[i]);
                }
            }
            Console.ReadLine();
        }
    }
}
