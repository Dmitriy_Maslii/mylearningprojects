﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FootballGame
{
    public static class ImprovedRandom
    {
        static Random random = new Random();

        public static bool GetRandom(int a, int b)
        {
            var randomNum = random.Next(a + b);
      
            if (a > b)
            {
                if (randomNum > b * 2) return true;
                if (randomNum % 2 == 0) return true;
                else return false;
            }
            if (a < b)
            {
                if (randomNum > a * 2) return false;
                if (randomNum % 2 == 0) return true;
                else return false;
            }
            if (randomNum % 2 == 0) return true;
            else return false;
        }
        public static int GetRandom(int a, int b, int c)
        {
            var a_b = GetRandom(a, b);
            var b_c = GetRandom(b, c);
            var c_a = GetRandom(c, a);
            
            while((!a_b && !b_c && !c_a) || (a_b && b_c && c_a))
            {
                a_b = GetRandom(a, b);
                b_c = GetRandom(b, c);
                c_a = GetRandom(c, a);
            }

            if (a_b && !c_a) return 1;
            if (b_c && !a_b) return 2;
            if (c_a && !b_c) return 3;
            return 0;
        }
    }
}
