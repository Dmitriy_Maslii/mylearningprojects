﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace FootballGame
{
    public class Team
    {
        public Team(String name, List<Player> team)
        {
            Name = name;
            playersOfTeam = team;
        }
        public List<Player> playersOfTeam
        {
            get;
        }
        public string Name
        {
            get;
        }
    }
}
