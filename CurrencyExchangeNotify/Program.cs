﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;

namespace CurrencyExchangeNotify
{
    class Program
    {
        private static Dictionary<string, Func<string, decimal?>> sources = new Dictionary<string, Func<string, decimal?>>()
        {
            { "https://minfin.com.ua/currency/kharkov/usd/", x => ParseFirstSource(x) },
            {"https://www.prostobank.ua/spravochniki/kursy_valyut", x => ParseSecondSource(x) }
        };

        static void Main(string[] args)
        {
            
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                                 | SecurityProtocolType.Tls11
                                                 | SecurityProtocolType.Tls12
                                                 | SecurityProtocolType.Ssl3;

            var setRates = new List<decimal>();
            decimal previousAverageRate = 0;

            while (true)
            {
                setRates.Clear();

                foreach (var source in sources)
                {
                    string result;
                    using (var client = new WebClient())
                    {
                        client.Headers.Add("user-agent", "Only a test!");

                        client.Encoding = Encoding.UTF8;
                        result = client.DownloadString(source.Key);
                    }

                    var rate = source.Value(result);
                    if (rate != null)
                    {
                        setRates.Add(rate.Value);
                    }
                }

                var averageRate = setRates.Average();
                if (Math.Abs(previousAverageRate - averageRate) >= 0.01m)
                {
                    Notify();
                    WriteToFile(averageRate);
                   
                }

                Console.WriteLine(averageRate);
                previousAverageRate = averageRate;
                Thread.Sleep(10000);
            }
        }
        private static void WriteToFile(decimal rate)
        {
            File.AppendAllText(@"../../Rates.txt", $"{DateTime.Now:HH:mm:ss} --- {rate:f4}\n");
        }
        private static void Notify()
        {
            for (int i = 0; i < 3; i++)
            {
                Console.Beep();
                Thread.Sleep(500);
            }
            Console.Beep(1000, 1000);
        }
        private static decimal? ParseFirstSource(string a)
        {
            a = a.Substring(a.IndexOf("<a href=\"/currency/nbu/usd/\">НБУ</a>"));
            var str = "<span class=\"mfm-posr\">";
            a = a.Substring(a.IndexOf(str) + str.Length, 7);
            a = a.Replace('.', ',');
            
            decimal result;
            if(decimal.TryParse(a, out result))
            {
                
                return result;
            }            
            return null;
        }
        private static decimal? ParseSecondSource(string a)
        {
            a = a.Substring(a.IndexOf("доллар США"));
            var str = "<div\nclass=\"col col-currency-rate\"><p>";
            a = a.Substring(a.IndexOf(str) + str.Length, 6);
            
            decimal result;
            if (decimal.TryParse(a, out result))
            {
                return result;
            }
            return null;
        }
    }
}

