﻿using System;
using GerericReport.Models;
using GerericReport.Reports;

namespace GerericReport.Infrastructure
{
    public static class ReportGenerator
    {
        public static IReport<T> Generate<T>(T entity) where T : Person
        {
            IReport<T> result = null;
            if (entity is Student)
            {
                result = new StudentReport().Generate(entity as Student) as IReport<T>;
            }
            else
            {
                result = new WorkerReport().Generate(entity as Worker) as IReport<T>;
            }
            return result;
        }

        /// <summary>
        /// Выводит информацию на консоль
        /// </summary>
        public static void Display<T>(IReport<T> report) where T : Person
        {
            // нужно реализовать вывод информации на консоль: для отчета по студенту и по работнику (всю информацию, которая есть в отчете)
            if(report is StudentReport)
            {
                Console.WriteLine("STUDENT REPORT:");
                StudentReport studentReport = (StudentReport)report;
                Console.WriteLine($"Самая высокая оценка: {studentReport.MaxMark}");
                Console.WriteLine($"Самая низкая оценка: {studentReport.MinMark}");
                Console.WriteLine($"Средняя оценка: {studentReport.AverageMark}");
                Console.WriteLine($"Количество полученных оценок: {studentReport.MarksCount}");
                
                if (studentReport.WeakSubject.Split(' ').Length == 1) 
                    Console.WriteLine($"Дисциплина, по которой наихудшая успеваемость: {studentReport.WeakSubject}");
                else Console.WriteLine($"Дисциплины, по которым наихудшая успеваемость: {studentReport.WeakSubject}\n");
            }
            else
            {
                Console.WriteLine("WORKER REPORT:");
                WorkerReport workerReport = (WorkerReport)report;
                Console.WriteLine($"Заработная плата: { workerReport.Salary}");
                Console.WriteLine($"Уровень надежности работника: { workerReport.Level}\n"); 
            }
        }
    }
}
