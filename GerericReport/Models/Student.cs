﻿using System;
using System.Collections.Generic;

namespace GerericReport.Models
{
    public class Student : Person
    {
        public Student(DateTime startEducationOn, List<Mark> marks = null)
        {
            StartDate = startEducationOn;
            Marks = marks ?? new List<Mark>();
        }

        /// <summary>
        /// Дата, когда поступил на учебу
        /// </summary>
        public DateTime StartDate { get; }

        /// <summary>
        /// Оценки по разным предметам
        /// </summary>
        public List<Mark> Marks { get; }

        /// <summary>
        /// Время обучения в годах
        /// </summary>
        public int EducationDuration => (int)(DateTime.Now - StartDate).TotalDays / 365;
    }

    /// <summary>
    /// Оценка по учебной дисциплине
    /// </summary>
    public class Mark
    {
        /// <param name="subject">Название дисциплины</param>
        /// <param name="date">Дата, когда поставили оценку</param>
        /// <param name="mark">Оценка по пятибальной шкале</param>
        public Mark(string subject, DateTime date, int mark)
        {
            Subject = subject;
            Date = date;
            Value = mark;
        }

        /// <summary>
        /// Название дисциплины
        /// </summary>
        public string Subject { get; }

        /// <summary>
        /// Дата, когда поставили оценку
        /// </summary>
        public DateTime Date { get; }

        /// <summary>
        /// Оценка по пятибальной шкале
        /// </summary>
        public int Value { get; }
    }
}
