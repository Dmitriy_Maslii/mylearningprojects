﻿namespace GerericReport.Models
{
    public class Worker : Person
    {
        public Worker(decimal salary, int daysOff)
        {
            Salary = salary;
            DaysOffCount = daysOff;
        }

        /// <summary>
        /// Зарплата
        /// </summary>
        public decimal Salary { get; }

        /// <summary>
        /// Количество использованных дней отпуска
        /// </summary>
        public int DaysOffCount { get; }
    }
}
