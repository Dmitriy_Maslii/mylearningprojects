﻿using GerericReport.Infrastructure;
using GerericReport.Models;
using System;
using System.Collections.Generic;

namespace GerericReport
{
    class Program
    {
        //Почему в моих проектах консоль после выполнения программы не закрывается, а GenericReport, созданный не мной, закрывает?
        static void Main(string[] args)
        {
            var persons = new List<Person>
            {
                new Student(
                   new DateTime(2018, 9, 1),
                   new List<Mark>
                   {
                       new Mark("История", new DateTime(2018, 10, 1), 5),
                       new Mark("География", new DateTime(2018, 10, 1), 5),
                       new Mark("История", new DateTime(2018, 11, 1), 3),
                       new Mark("Биология", new DateTime(2018, 12, 1), 5),
                       new Mark("Математика", new DateTime(2019, 1, 1), 3),
                   })
                {
                   Name = "Петя Иванов"
                },
                new Worker(1000, 15),
            };

            var studentReport = ReportGenerator.Generate(persons[0] as Student);

            // todo: реализовать метод
            ReportGenerator.Display(studentReport);

            var workerReport = ReportGenerator.Generate(persons[1] as Worker);

            // todo: реализовать метод
            ReportGenerator.Display(workerReport);

            Console.ReadKey();
        }
    }
}
