﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyLibCollections.MyList;

namespace MyLibCollections.Tests
{
    [TestClass]
    public class ListTests
    {
        [TestMethod]
        public void AddTest()
        {
            //arrange
            List<int> a = new List<int>();

            //act
            a.Add(3);
            a.Add(5);

            //assert
            Assert.AreEqual(a[0], 3);
            Assert.AreEqual(a[1], 5);
            Assert.AreEqual(a.Capacity, 4);
        }
        [TestMethod]
        public void AddRangeTest()
        {
            List<int> a = new List<int>();
            a.Add(3);

            System.Collections.Generic.IEnumerable<int> enumerable = new System.Collections.Generic.List<int>() { 5, 7, 3, 9 };
            a.AddRange(enumerable);

            Assert.AreEqual(a[0], 3);
            Assert.AreEqual(a[1], 5);
            Assert.AreEqual(a[2], 7);
            Assert.AreEqual(a[3], 3);
            Assert.AreEqual(a[4], 9);
            Assert.AreEqual(a.Capacity, 8);
        }
        [TestMethod]
        public void ContainsTest()
        {
            List<string> list = new List<string>();

            list.Add("asdf");
            list.Add("qwer");
            list.Add("zxcv");

            Assert.IsTrue(list.Contains("asdf"));
            Assert.IsTrue(list.Contains("qwer"));
            Assert.IsTrue(list.Contains("zxcv"));
            Assert.IsFalse(list.Contains("poiu"));
        }
        [TestMethod]
        public void ExistsTest()
        {
            List<int> list = new List<int>();

            list.Add(34);
            list.Add(21);
            list.Add(78);

            Assert.IsTrue(list.Exists(x => x == 34));
            Assert.IsTrue(list.Exists(x => x == 78));
            Assert.IsFalse(list.Exists(x => x == 43));
        }
        [TestMethod]
        public void RemoveTest()
        {
            List<int> list = new List<int>();

            list.Add(10);
            list.Add(20);
            list.Add(30);
            list.Add(40);
            list.Add(50);

            Assert.IsTrue(list.Remove(20));
            Assert.IsTrue(list.Remove(40));
            Assert.AreEqual(list[2], 50);
            Assert.AreEqual(list[1], 30);
        }
    }
}
