﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyLibCollections.MyLinkedList;

namespace MyLibCollections.Tests
{
    [TestClass]
    public class LinkedListTests
    {        
        [TestMethod]
        public void AddFirstTest()
        {
            //arrenge
            LinkedList<int> list = new LinkedList<int>();

            //act
            list.AddFirst(1);
            list.AddFirst(2);
            list.AddFirst(3);
            list.AddFirst(4);
            list.AddFirst(5);
            list.AddFirst(6);
            list.AddFirst(7);
            list.AddFirst(8);

            //assert
            list.RemoveLast();
            Assert.AreEqual(8, list.First.Value);
            list.RemoveFirst();
            Assert.AreEqual(7, list.First.Value);
            list.RemoveLast();
            Assert.AreEqual(7, list.First.Value);
            list.RemoveFirst();
            Assert.AreEqual(6, list.First.Value);
            list.RemoveFirst();
            Assert.AreEqual(5, list.First.Value);
            list.RemoveLast();
            Assert.AreEqual(5, list.First.Value);
            list.RemoveFirst();
            Assert.AreEqual(4, list.First.Value);
        }
        [TestMethod]
        public void AddLastTest()
        {
            LinkedList<int> list = new LinkedList<int>();

            list.AddLast(1);
            list.AddLast(2);
            list.AddLast(3);
            list.AddLast(4);
            list.AddLast(5);
            list.AddLast(6);
            list.AddLast(7);
            list.AddLast(8);

            
            list.RemoveLast();
            Assert.AreEqual(1, list.First.Value);
            list.RemoveFirst();
            Assert.AreEqual(2, list.First.Value);
            list.RemoveLast();
            Assert.AreEqual(2, list.First.Value);
            list.RemoveFirst();
            Assert.AreEqual(3, list.First.Value);
            list.RemoveFirst();
            Assert.AreEqual(4, list.First.Value);
            list.RemoveLast();
            Assert.AreEqual(4, list.First.Value);
            list.RemoveFirst();
            Assert.AreEqual(5, list.First.Value);
        }
        [TestMethod]
        public void AddAfterTest()
        {
            LinkedList<int> list = new LinkedList<int>();

            list.AddLast(1);
            list.AddLast(2);
            list.AddLast(3);

            list.AddAfter(list.Find(2), 22);
            list.AddAfter(list.Find(3), 33);

            Assert.AreEqual(list.First.Value, 1);
            list.RemoveFirst();
            Assert.AreEqual(list.First.Value, 2);
            list.RemoveFirst();
            Assert.AreEqual(list.First.Value, 22);
            list.RemoveFirst();
            Assert.AreEqual(list.First.Value, 3);
            list.RemoveFirst();
            Assert.AreEqual(list.First.Value, 33);
            list.RemoveFirst();
        }
        [TestMethod]
        public void AddBeforTest()
        {
            LinkedList<int> list = new LinkedList<int>();

            list.AddLast(1);
            list.AddLast(2);
            list.AddLast(3);

            list.AddBefor(list.Find(1), 11);
            list.AddBefor(list.Find(2), 22);

            Assert.AreEqual(list.First.Value, 11);
            list.RemoveFirst();
            Assert.AreEqual(list.First.Value, 1);
            list.RemoveFirst();
            Assert.AreEqual(list.First.Value, 22);
            list.RemoveFirst();
            Assert.AreEqual(list.First.Value, 2);
            list.RemoveFirst();
            Assert.AreEqual(list.First.Value, 3);
            list.RemoveFirst();
        }
    }
}
