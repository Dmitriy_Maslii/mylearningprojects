﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLibCollections.MyLinkedList
{
    public class LinkedList<T>
    {
        public LinkedListNode<T> HeadNode
        {
            private set;
            get;
        }

        public LinkedListNode<T> First
        {
            get
            {
                return HeadNode;
            }
        }
        public void AddFirst(T value)
        {
            AddFirst(new LinkedListNode<T>(value));
        }
        public void AddFirst(LinkedListNode<T> node)
        {
            if (HeadNode == null)
            {
                HeadNode = node;
            }
            else 
            {
                HeadNode.Previous = node;
                node.Next = HeadNode;
                HeadNode = node;
            }
        }


        public void AddLast(T value)
        {
            AddLast(new LinkedListNode<T>(value));
        }
        public void AddLast(LinkedListNode<T> node)
        {
            if(HeadNode == null)
            {
                HeadNode = node;
            }
            else if(HeadNode.Next == null)
            {
                HeadNode.Next = node;
                node.Previous = HeadNode;
            }
            else
            {
                var lastNode = HeadNode;
                do
                {
                    lastNode = lastNode.Next;
                } while (lastNode.Next != null);

                lastNode.Next = node;
                node.Previous = lastNode; 
            }
        }
        public void RemoveFirst()
        {
            if (HeadNode == null)
            {
                throw new Exception("LinkedList is empty");
            }
            HeadNode = HeadNode.Next;
            if (HeadNode != null)
            {
                HeadNode.Previous = null;
            }
        }
        public void RemoveLast()
        {
            if(HeadNode.Next == null)
            {
                HeadNode = null;
            }
            else
            {
                var penultimateNode = HeadNode;
                while(penultimateNode.Next.Next != null)
                {
                    penultimateNode = penultimateNode.Next;
                }
                penultimateNode.Next = null;
            }

        }
        public void AddBefor(LinkedListNode<T> node, T value)
        {
            AddBefor(node, new LinkedListNode<T>(value));
        }
        public void AddBefor(LinkedListNode<T> node, LinkedListNode<T> newNode) 
        {
            newNode.Next = node;
            newNode.Previous = node.Previous;
            if(node.Previous != null)
            {
                node.Previous.Next = newNode;                
            }
            else
            {
                HeadNode = newNode;
            }
            node.Previous = newNode;
        }
        public void AddAfter(LinkedListNode<T> node, T value)
        {
            AddAfter(node, new LinkedListNode<T>(value));
        }
        public void AddAfter(LinkedListNode<T> node, LinkedListNode<T> newNode)
        {
            newNode.Previous = node;
            newNode.Next = node.Next;
            if(node.Next != null)
            {
                node.Next.Previous = newNode;
            }
            node.Next = newNode;
        }
        public LinkedListNode<T> Find(T value)
        {
            var TempNode = HeadNode;
            
            while(TempNode != null)
            {
                if (TempNode.Value.Equals(value))
                {
                    return TempNode;
                }
                TempNode = TempNode.Next;
            }
            return null;
        }
    }
   
    
    
    public class LinkedListNode<T>
    {
        public LinkedListNode(T value)
        {
            Value = value;
        }
        public T Value
        {
            get;
            set;
        }
        public LinkedListNode<T> Next
        {
            get;
            set;
        }
        public LinkedListNode<T> Previous
        {
            get;
            set;
        }
    }
}
