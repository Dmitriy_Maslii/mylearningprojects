﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLibCollections.MyList
{
    public class List<T>
    {
        private T[] _items;
        private int _size;

        public List() : this(0)
        {
        }
        public List(int capacity)
        {
            _items = new T[capacity];
        }



        public void Add(T item)
        {
            if (_items.Length == _size)
            {
                IncreaseCapacity();
            }
            _items[_size] = item;
            _size++;
        }
        public void AddRange(IEnumerable<T> collection)
        {
            while (_items.Length - _size < collection.Count())
            {
                IncreaseCapacity();
            }
            Array.Copy(collection.ToArray(), 0, _items, _size, collection.Count());
            _size += collection.Count();
        }
        public bool Contains(T item)
        {
            for (int i = 0; i < _size; i++)
            {
                if (EqualityTest(_items[i], item))
                {
                    return true;
                }
            }
            return false;
        }
        public bool Exists(Predicate<T> predicate)
        {
            for (int i = 0; i < _items.Length; i++)
            {
                if (predicate(_items[i]))
                {
                    return true;
                }
            }
            return false;
        }
        public List<T> GetRange(int index, int count)
        {
            List<T> list = new List<T>(count);
            int indexLastItem = index + count;
            for (; index < indexLastItem; index++)
            {
                list.Add(_items[index]);
            }
            return list;
        }
        public int IndexOf(T item)
        {
            for (int i = 0; i < _items.Length; i++)
            {
                if (EqualityTest(_items[i], item))
                {
                    return i;
                }
            }
            return -1;
        }
        public void Insert(int index, T item)
        {
            if (_size == _items.Length)
            {
                IncreaseCapacity();
            }
            _size++;

            for (; index <= _size; index++)
            {
                T temp = _items[index];
                _items[index] = item;
                item = temp;
            }
        }
        public void InsertRange(int index, IEnumerable<T> collection)
        {
            foreach (T item in collection)
            {
                Insert(index, item);
                index++;
            }
        }
        public bool Remove(T item)
        {
            for (int i = 0; i < _items.Length; i++)
            {
                if (EqualityTest(_items[i], item))
                {
                    RemoveAt(i);
                    return true;
                }
            }
            return false;
        }
        public void RemoveAt(int index)
        {
            for (; index < _items.Length - 1; index++)
            {
                _items[index] = _items[index + 1];
            }
            _size--;
            _items[_size] = default(T);
        }
        public void RemoveRange(int index, int count)
        {
            for (int i = 0; i < count; i++)
            {
                RemoveAt(index);
            }
        }
        public T this[int index]
        {
            set
            {
                _items[index] = value;
            }
            get
            {
                return _items[index];
            }
        }
        public int Capacity
        {
            get
            {
                return _items.Length;
            }
        }








        private bool EqualityTest(T firstItem, T secondItem)
        {
            if (firstItem.Equals(secondItem))
            {
                return true;
            }
            return false;
        }
        private void IncreaseCapacity()
        {
            if (_items.Length == 0)
            {
                _items = new T[4];
            }
            else
            {
                T[] newItems = new T[_items.Length * 2];
                Array.Copy(_items, newItems, _size);
                _items = newItems;
            }
        }
    }
}
