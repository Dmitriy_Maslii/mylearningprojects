﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Experiments
{        
    class Program
    {
        static void Main(string[] args)
        {
            List<Action> actions = new List<Action>();
            for (var count = 0; count < 10; count++)
            {
                var a = count;
                actions.Add(() => Console.WriteLine(a));
            }
            foreach (var action in actions)
            {
                action();
            }
            Console.ReadLine();
        }
    }
}
